<?php 
/**
 * Plugin Name: DD Heap WordPress Integration
 * Plugin URI: https://bitbucket.org/d2roth/dd-heap-wp-integration
 * Description: This plugin adds the Heap snippet to your WordPress site. Remember to add <code>YOUR_APP_ID</code> under Settings > General > <code>HEAP APP_ID</code>.
 * Version:     0.1.0
 * Author:      Daniel Roth
 * License:     GPL
 */

if(!class_exists('DD_Heap_Analytics')){
	
	class DD_Heap_Analytics{
		
		public function __construct(){
			add_filter('admin_init', [$this, 'register_fields']);
			add_action( 'wp_head', [$this, 'dd_heap_add_snippet_to_head'], 9999 );
		}
		
		/**
		 * Adds the Heap analytics snippet to the site's <head>
		 *
		 * @since  0.1.0
		 *
		 * @return void
		 */
		public function dd_heap_add_snippet_to_head() {
			if ( ! ( $heap_app_id = get_option('dd_heap_app_id') ) ) {
				return;
			}
			?>
			<script type="text/javascript">
			  window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
			  heap.load("<?php echo esc_attr( $heap_app_id ); ?>");
			</script>
			<?php
		}

		/**
		 * Registers the dd_heap_app_id field and adds it to the general settings page
		 *
		 * @since  0.1.0
		 *
		 * @return void
		 */
		public function register_fields(){
			register_setting('general', 'dd_heap_app_id', 'esc_attr');
			add_settings_field('dd_heap_app_id', '<label for="dd_heap_app_id">'. __('HEAP APP_ID' , 'dd_heap_analytics' ).'</label>' , [$this, 'print_app_id_field'], 'general');
		}
		
		/**
		 * Prints the dd_heap_app_id field in all it's glory
		 *
		 * @since  0.1.0
		 *
		 * @return void
		 */
		public function print_app_id_field(){
			$value = get_option( 'dd_heap_app_id', '' );
			echo '<input type="text" id="dd_heap_app_id" name="dd_heap_app_id" placeholder="YOUR_APP_ID" value="' . $value . '" />';
			echo '<p class="description" id="dd_heap_app_id-description">This must be set to the ID of the environment to which you want to send data. You can find this ID within your <a href="https://heapanalytics.com/app/account" target="_blank">Account Settings</a> under the Project tab.</p>';
		}

	}

}

new DD_Heap_Analytics();
